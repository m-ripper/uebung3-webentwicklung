class CreateContracts < ActiveRecord::Migration[5.2]
  def change
    create_table :contracts do |t|
      t.string :name
      t.string :description
      t.references :customer, foreign_key: true
      t.datetime :dateStart
      t.datetime :dateEnd

      t.timestamps
    end
  end
end
