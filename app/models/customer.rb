class Customer < ApplicationRecord
  has_many :contracts, dependent: :destroy

  validates :firstName, presence: true, length: {minimum: 3}
  validates :name, presence: true, length: {minimum: 3}
  validates :salary, presence: true, length: {minimum: 1}

end
