class Contract < ApplicationRecord
  belongs_to :customer

  after_initialize :set_default_start_time, :if => :new_record?
end

private

def set_default_start_time
  self.dateStart = Time.now
end
