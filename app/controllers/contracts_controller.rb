class ContractsController < ApplicationController
  def create
    @customer = Customer.find(params[:customer_id])
    @contract = @customer.contracts.create(contract_params)
    redirect_to customer_path(@customer)
  end

  def destroy
    @customer = Customer.find(params[:customer_id])
    @contract = @customer.contracts.find(params[:id])
    @contract.destroy
    redirect_to customer_path(@customer)
  end

  private
  def contract_params
    params.require(:contract).permit(:name, :description, :dateEnd)
  end
end
