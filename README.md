# Uebung3 (Webentwicklung)
## Beschreibung
Objekt dieser Aufgabe war es Grundwissen ueber Ruby on Rails anzuwenden, so sollten 2 Modelle definiert und nach RESTful-Prinzipien erstellt, angezeigt, aktualisiert und gel�scht werden. (CRUD)
Dabei sollten die Modelle in einer Relation stehen.

Die App kann auch [hier](https://uebung3-test-app.herokuapp.com/) angeguckt werden.

## Vorraussetzungen
Um dieses Programm nutzen zu koennen wird gebraucht:

1. Eine aktuelle Version von Ruby gebraucht. Ruby kann von `https://www.ruby-lang.org/de/downloads/`.
2. Bei der Abfrage von `ruby -v` muss eine Version ausgegeben werden.

## Installation
Sofern die Vorraussetzungen eingehalten worden sind, kann man dieses Repository an eine beliebige Stelle clonen.

## Benutzung
1. Per Kommandozeile in den Ordner des Repositories wechseln.
2. Mithilfe von `bin/rails db:migrate` die Datenbank initialisieren, falls nicht Windows genutzt wird, kann `ruby` im Befehl weggelassen werden.
3. Mithilfe von `ruby bin\rails server` den Server starten.
4. Zugriff auf den Server findet ueber einen Webbrowser unter der Adresse `http://localhost:3000` statt.

