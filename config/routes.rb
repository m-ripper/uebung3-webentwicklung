Rails.application.routes.draw do

  get 'default/index'
  resources :customers do
    resources :contracts
  end

  root 'default#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
